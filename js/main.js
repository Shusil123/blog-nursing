$(document).ready(function() {
 $(".button-collapse").sideNav({
 	menuWidth: 200
 });
 new WOW().init();
 $('.parallax').parallax();
 $('select').material_select();
  $('input.autocomplete').autocomplete({
  	data: {
  		"College of Nursing": null,
  		"St. Johns Meical College Hospital": null,
  		"M.S. Ramaiah Medical College & Hospital": null,
  		"Oxford Nursing College": null,
  		"Florence Nursing College": null,
  		"Faran Nursing College": null,
  		"K.M.J Reddy College of Nursing": null,
  		"Swamy Vivekananda College of Nursing": null,
  		"K.T.G College of Nursing": null,
  		"St. Marthas Hospital's College of Nursing": null,
  		"Sri Raghavendra College of Nursing": null,
  		"East West College of Nursing": null,
  		"Sarvodaya College of Nursing": null,
  		"St. Philomena's College of Nursing": null,
  		"Hillside College of Nursing": null,
  		"Goutham College of Nursing": null,
  		"K.V.S College of Nursing": null,
  		"Kempegowda college of Nursing": null,
  		"B.J.R College of Nursing": null,
  		"Mallige College of Nursing": null,
  		"Shekar College of Nursing": null,
  		"Maruti College of Nursing": null,
  		"Miranda College of Nursing": null,
  		"Bangalore City College of Nursing": null,
  		"Rajiv Gandhi College of Nursing": null,
  		"Sneha College of Nursing": null,
  		"Global College of Nursing": null,
  		"Padmashree College of Nursing": null,
  		"S.G.R College of Nursing": null,
  		"Dayanand Sagar College of Nursing": null,
  		"Vijayanagar College of Nursing": null,
  		"S.B Institute of Nursing": null,
  		"Mother Theresa College of Nursing": null,
  		"Gayathri College of Nursing": null,
  		"St. George College of Nursing": null,
  		"Chinai College of Nursing": null,
  		"Sri Varalakshmi College of Nursing": null,
  		"Vidya Kirana Institute of Nursing Sciences": null,
  		"Diana Education Trust, Institute of Nursing": null,
  		"AVK College of Nursing": null,
  		"Eben Ezer College of Nursing": null,
  		"Florida College of Nursing": null,
  		"T. John College of Nursing": null,
  		"Nightingale College of Nursing": null,
  		"Narayana Hrudayalaya": null,
  		"Bhagwan Mahaveer Jain College of Nrusing": null,
  		"Vydehi Institute of Nursing": null,
  		"Sofia College of Nursing": null
  	}
  });
});